import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { Http } from '@angular/http';

import { Storage } from "@ionic/Storage";
import { Provider } from '../../providers/provider/provider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  username: string;
  password: string;
  loader: any;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: Provider,
    private storage: Storage,
    public http:Http) {

  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
    });
    return await this.loader.present();
  }

  presentToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  login() {
    this.presentLoading();
    let body =  {  
                    "action":"cek_login",
                    "username":this.username,
                    "password":this.password
                }; 
    this.postPvdr.postData(body).subscribe((res) => {
      
      if(res.status == 200 && res.message == 'success'){
        this.storage.set('username',res.data.username);
        this.storage.set('nama',res.data.employee_name);
        this.navCtrl.push(DashboardPage);
      }else{
        this.presentToast('Correct username and password');
      }
      this.loader.dismiss();
    }, error => {
      this.presentToast("Connection Failed, Try again.");
      this.loader.dismiss();
    });

  }
}
