import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { CheckinPage } from '../checkin/checkin';
import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history';
import { Storage } from "@ionic/Storage";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  date = new Date();
  myDate: String = new Date(this.date.getTime() - this.date.getTimezoneOffset() * 60000).toISOString();
  todayDate: String = new Date().toISOString();
  daylight: any;
  username:any;
  nama:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    private storage: Storage
  ) {

    var time = this.myDate.substring(11, 19)
    if (time >= '00:00:01' && time <= '10:00:00') {
      this.daylight = 'Pagi';
    } else if (time >= '10:00:00' && time <= '15:00:00') {
      this.daylight = 'Siang';
    } else if (time >= '15:00:00' && time <= '18:00:00') {
      this.daylight = 'Sore';
    } else if (time > '18:00:00') {
      this.daylight = 'Malam';
    }

    this.storage.get("username").then((res) => {
        this.username =res;
    });
    this.storage.get("nama").then((res2) => {
        this.nama =res2;
    });

    console.log(this.todayDate);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  goToCheckIn() {
    this.navCtrl.push(CheckinPage);

  }
  goToHistory() {
    this.navCtrl.push(HistoryPage);
  }

  showActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: '',
      buttons: [
        // {
        //   text: 'Destructive',
        //   role: 'destructive',
        //   handler: () => {
        //     console.log('Destructive clicked');
        //   }
        // }, {
        //   text: 'Archive',
        //   handler: () => {
        //     console.log('Archive clicked');
        //   }
        // }, 
        {
          text: 'Logout',
          role: 'logout',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });
    actionSheet.present();
  }

}
