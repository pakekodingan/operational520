import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Provider } from '../../providers/provider/provider';
import { Storage } from "@ionic/Storage";
import { DashboardPage } from '../dashboard/dashboard';
declare var google;
@IonicPage()
@Component({
  selector: 'page-checkin',
  templateUrl: 'checkin.html',
})
export class CheckinPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  todayDate: String = new Date().toISOString();
  lat:any;
  lng:any;
  alamat: String;
  loader : any;
  username : any;
  constructor(
      public navCtrl: NavController, 
      public toastCtrl: ToastController,
      public navParams: NavParams,
      public geo: Geolocation,
      private androidPermissions: AndroidPermissions,
      private locationAccuracy : LocationAccuracy,
      public loadingCtrl: LoadingController,
      public postPvdr: Provider,
      private storage: Storage,
      public http:Http
    ) {
  }

  ionViewDidLoad() {
    this.checkGPSPermission(); 
  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
    });
    return await this.loader.present();
  }

  presentToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  ionViewDidEnter(){
    this.geo.getCurrentPosition().then(pos => {
      this.lat = pos.coords.latitude;
      this.lng = pos.coords.longitude;
      this.loadMap(this.lat,this.lng);
    }).catch( err => console.log(err));
  }

  loadMap(LAT,LNG){

    let latLng = new google.maps.LatLng(LAT, LNG);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.getAddress();
  }
  
  getAddress(){

     var geocoder = new google.maps.Geocoder;
     var latitude = this.lat;
     var longitude = this.lng;
     var gps = {lat:parseFloat(latitude),lng:parseFloat(longitude)};
     geocoder.geocode({'location':gps},function (result,status){
        if(status =='OK'){
          if(result[0]){
            var inputElement = <HTMLInputElement>document.getElementById('address');
            inputElement.value = result[0].formatted_address;
          }
        }
     });
     this.addMarker();
  }
  
  addMarker(){

    var myLatlng = { lat: this.lat, lng: this.lng };
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: myLatlng
    });
  }

  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();

        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();

        }
      },
      err => {
        console.log(err);
      }
    );

  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();

            },
            error => {
              //Show alert if user click on 'No Thanks'
              console.log('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        
      },
      error => console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }
  
  checkin(){
    this.presentLoading();
    this.storage.get("username").then((res) => {
        let body =  {  
          "action":"checkin",
          "latitude":this.lat,
          "longitude":this.lng,
          "username":res,
          "tanggal":"2020-08-13",
          "waktu":"12:90:00",
          "alamat":"test"
        };
        this.postPvdr.postData(body).subscribe((res) => {
      
          if(res.status == 200 && res.message == 'success'){
            
            this.presentToast('Success');
            this.navCtrl.push(DashboardPage);
          }else{
            this.presentToast('Failed');
          }
          this.loader.dismiss();
        }, error => {
          this.presentToast("Connection Failed, Try again.");
          this.loader.dismiss();
        }); 
    });
  }
}
