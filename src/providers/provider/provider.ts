
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Provider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Provider {

	server: string = "https://operational520.com/api_mobile.php";
	constructor(public http: Http) {

	}

	postData(body) {
		let type = "text/plain";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server, JSON.stringify(body), options)
			.map(res => res.json());
	}

}
