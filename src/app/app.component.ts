import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/Storage";
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private storage: Storage) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // login session
    this.storage.get("username").then((res) => {
      if (res == null) {
        this.rootPage = HomePage;
      } else {
        this.rootPage = DashboardPage;
      }
    });
  }

  // prepareTable() {

	// 	this.sqlite.create({
	// 		name: 'operaional520.db',
	// 		location: 'default'
	// 	}).then((db: SQLiteObject) => {

	// 		//table setting
	// 		db.executeSql('CREATE TABLE IF NOT EXISTS login (username TEXT, nama TEXT)', [])
	// 			.then(res => console.log('Successfully Create Table setting'))
	// 			.catch(e => console.log(e));

	// 	}).catch(e => console.log(e));

	// }
}

